
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(defvar my-packages '(org company helm helm-org tramp which-key helpful helm-company company-org-block magit))

(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))
